'''
Created on Jun 16, 2013

@author: michaelbessey
'''

import os
import jinja2

from com.msb.base.webapp2.handler import Handler

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + '/views'),
    extensions=['jinja2.ext.autoescape'])


class BaseHandler(Handler):

    def render_view(self, filename, template_values = {}):
        self.render_template(jinja_environment, filename, template_values)
        
        
class Information :
    
    def __init__(self):
        self.version = 1.0
        self.server = 'local'