'''
Created on Jun 16, 2013

@author: michaelbessey
'''

from app import BaseHandler

class IndexHandler(BaseHandler):

    def get(self):
        self.render_view('index.html')

class InformationHandler(BaseHandler):

    def get(self):
        self.render_view('information.html')