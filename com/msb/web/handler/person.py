'''
Created on Jun 16, 2013

@author: michaelbessey
'''

from com.msb.web.data.person import PersonDao
from com.msb.web.data.person import PersonView
from com.msb.web.form.person import PersonForm

from app import BaseHandler

class PersonRestHandler(BaseHandler):
    
    def get(self):
        
        personId = int(self.request.get('id'))
        
        personDao = PersonDao()
        personModel = personDao.getPerson(personId)
        
        person = PersonView(personModel)
        
        self.render_json(person)
        

class PersonListHandler(BaseHandler):
    
    def get(self):
        
        personDao = PersonDao()
        
        personList = personDao.queryPersonList()
        
        template_values = {
            'personList': personList
        }
        
        self.render_view('person/index.html', template_values)
        

class PersonCreateHandler(BaseHandler):

    def get(self):
        
        personForm = PersonForm()

        template_values = {
            'personForm': personForm,
            'errorDict': {}
        }
        
        self.render_view('person/create.html', template_values)
    
    
    def post(self):

        personForm = PersonForm(self.request)
        errorDict = personForm.validate()

        view = 'person/create.html'

        if len(errorDict) <= 0 :
            
            personModel = personForm.toPersonModel()
            personModel.put()
            return self.redirect('/person/list')
        
        template_values = {
            'personForm': personForm,
            'errorDict': errorDict,
        }

        self.render_view(view, template_values)
        

class PersonDeleteHandler(BaseHandler):
    
    def get(self):
        
        personId = int(self.request.get('id'))
        
        personDao = PersonDao()
        personDao.delete(personId)
        
        return self.redirect('/person/list')
        