'''
Created on Jun 16, 2013

@author: michaelbessey
'''

from com.msb.web.form.message import MessageForm

from app import BaseHandler

class ContactHandler(BaseHandler):

    def get(self):

        messageForm = MessageForm()

        template_values = {
            'messageForm': messageForm,
            'errorDict': {}
        }

        self.render_view('contact/index.html', template_values)

    def post(self):

        messageForm = MessageForm(self.request)
        errorDict = messageForm.validate()

        view = 'contact/submit.html'

        if len(errorDict) > 0 :
            view = 'contact/index.html'
        else :
            messageForm.sendMessage()
            message = messageForm.toMessage()
            message.put()


        template_values = {
            'messageForm': messageForm,
            'errorDict': errorDict,
        }

        self.render_view(view, template_values)
