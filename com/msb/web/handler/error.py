'''
Created on Jun 16, 2013

@author: michaelbessey
'''

import logging

from app import BaseHandler

class Error404Handler(BaseHandler):

    def get(self):
        exception=self.request.route_args['exception']
        logging.exception(exception)
        
        self.response.set_status(404)
        self.render_view('error/error404.html')


class Error500Handler(BaseHandler):

    def get(self):
        exception=self.request.route_args['exception']
        logging.error(exception)
        
        self.response.set_status(500)
        self.render_view('error/error500.html')