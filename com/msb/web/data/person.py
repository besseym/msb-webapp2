'''
Created on Jun 16, 2013

@author: michaelbessey
'''

from google.appengine.ext import db
from com.msb.base.engine.db import BaseModel

class PersonDao :
    
    def queryPersonList (self) :
        return PersonModel.all().fetch(100, 0)
        #q = db.GqlQuery("select * from PersonModel")
        #return q.fetch(100, 0)
        
    def getPerson(self, personId):
        key = db.Key.from_path('PersonModel', personId)
        return db.get(key)
    
    
    def getId(self, model):
        return db.Key.id_or_name(model)
    
    
    def delete(self, personId):
        key = db.Key.from_path('PersonModel', personId)
        db.delete(key)


class PersonView :
    
    def __init__(self, personModel):
        self.id = personModel.key().id()
        self.name = personModel.name
        self.email = personModel.email


class PersonModel(BaseModel):
    name = db.StringProperty(required=True)
    email = db.EmailProperty(required=True)
    birthdate = db.DateProperty()
    phone= db.PhoneNumberProperty()
    gender = db.StringProperty(choices=set(["male", "female"]))
    likesIceCream = db.BooleanProperty()
        