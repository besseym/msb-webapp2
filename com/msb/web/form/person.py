'''
Created on Jun 16, 2013

@author: michaelbessey
'''

import datetime

from com.msb.base.web.form import Form
from com.msb.web.data.person import PersonModel

class PersonForm(Form):
    
    def __init__(self, request = None):

        if request != None :
            self.name = self.clean(request.get('name'))
            self.email = self.clean(request.get('email'))
            self.phone = self.clean(request.get('phone'))
            self.birthYear = self.clean(request.get('birthYear'))
            self.birthMonth = self.clean(request.get('birthMonth'))
            self.birthDay = self.clean(request.get('birthDay'))
            self.gender = self.clean(request.get('gender'))
    
    
    def validate(self):
        errorDict = {}
        
        self.validateDate('birthday', self.birthYear, self.birthMonth, self.birthDay, errorDict)
        
        self.validateEmail('email', self.email, errorDict)
        
        self.validateRequired('name', self.name, errorDict)
        self.validateRequired('email', self.email, errorDict)

        return errorDict
    
    
    def getBirthdate(self):
        return datetime.date(int(self.birthYear), int(self.birthMonth), int(self.birthDay))
    
    
    def toPersonModel(self):
        personModel = PersonModel(
                            name = self.name,
                            email = self.email,
                            phone = self.phone,
                            gender = self.gender,
                            birthdate = self.getBirthdate())
        return personModel