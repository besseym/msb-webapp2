'''
Created on Jun 17, 2013

@author: michaelbessey
'''

import datetime

from google.appengine.ext import db
    

class BaseModel(db.Model):
    createdAt = db.DateTimeProperty()
    updatedAt = db.DateTimeProperty()
    
    def put(self):
        if not self.has_key() :
            self.createdAt = datetime.datetime.now()
        
        self.updatedAt = datetime.datetime.now()
        super(BaseModel, self).put()