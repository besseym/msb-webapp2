'''
Created on Jun 16, 2013

@author: michaelbessey
'''

import json
import webapp2

class Handler(webapp2.RequestHandler):
    
    def render_template(self, template_environment, filename, template_values = {}):
        template = template_environment.get_template(filename)
        self.response.write(template.render(template_values))
        
    def render_json(self, obj):
        self.response.headers['Content-Type'] = 'application/json'
        self.response.out.write(json.dumps(obj.__dict__))