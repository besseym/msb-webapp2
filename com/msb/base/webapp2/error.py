'''
Created on Jun 16, 2013

@author: michaelbessey
'''

import webapp2
import logging

class ErrorHandlerAdapter(webapp2.BaseHandlerAdapter):

    def __call__(self, request, response, exception):
        logging.exception(exception)
        request.route_args = {}
        request.route_args['exception'] = exception
        handler = self.handler(request, response)
        return handler.get()