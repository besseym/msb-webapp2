import re
import cgi

import logging

import datetime

#from google.appengine.api import mail

class Form:

	def clean(self, value):
		return cgi.escape(value)

	def validateRequired(self, field, value, errorDict):
		if value == '':
			errorDict[field] = 'Field {} is required'.format(field)

	def validateMaxLength(self, field, value, length, errorDict):
		if len(value) > length:
			errorDict[field] = 'Field {} needs to be less or equal to {} characters in length'.format(field, length)

	def validateMinLength(self, field, value, length, errorDict):
		if len(value) < length:
			errorDict[field] = 'Field {} needs to be greater than or equal to {} characters in length'.format(field, length)

	def validateNumber(self, field, value, errorDict):
		if value.isdigit() != True :
			errorDict[field] = 'Field {} needs to be a valid numerical value'.format(field)

	def validateEmail(self, field, value, errorDict):
		if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", value) == None :
			errorDict[field] = 'Field {} is not a valid email address.'.format(field)
			
	def validateDate(self, field, year, month, day, errorDict):
		date = None
		
		try:
			date = datetime.date(int(year), int(month), int(day))
		except Exception as ex:
			logging.exception(ex)
			errorDict[field] = 'Field {} is not a valid date.'.format(field)
			
		return date
			
def main():
	errorDict = {}
	form = Form()
	form.validateEmail('email', 'me', errorDict)
	print(errorDict)

if __name__ == "__main__": main()
		

