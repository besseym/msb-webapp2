module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            scripts: {
                // the files to concatenate
                src: ['scripts-src/**/*.js'],
                // the location of the resulting JS file
                dest: 'scripts/<%= pkg.name %>.concat.js'
            },
            styles: {
                // the files to concatenate
                src: ['styles-src/**/*.css'],
                // the location of the resulting concatenated file
                dest: 'styles/<%= pkg.name %>.concat.css'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                files: {
                    'scripts/<%= pkg.name %>.min.js': ['scripts/<%= pkg.name %>.concat.js']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    banner: '/* My minified css file */'
                },
                files: {
                    'styles/<%= pkg.name %>.min.css': ['styles/<%= pkg.name %>.concat.css']
                }
            }
        },
        jshint: {
            // define the files to lint
            files: ['scripts-src/**/*.js'],
            // configure JSHint (documented at http://www.jshint.com/docs/)
            options: {
                // more options here if you want to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true
                }
            }
        },
        sass: {                              // Task
            dist: {                            // Target
                options: {                       // Target options
                    style: 'expanded'
                },
                files: [{
                    expand: true,
                    cwd: 'sass',
                    src: ['app.scss'],
                    dest: 'styles-src',
                    ext: '.css'
                }]
            }
        },
        watch: {
            js: {
                files: ['<%= jshint.files %>'],
                tasks: ['jshint'],
            },
            css: {
                files: ['sass/*.scss'],
                tasks: ['sass']
            }
        }
    });

    // Load the plugin that provides the tasks.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['uglify']);

};