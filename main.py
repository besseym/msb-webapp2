
import webapp2

from com.msb.base.webapp2.error import ErrorHandlerAdapter
from com.msb.web.handler.section import IndexHandler, InformationHandler
from com.msb.web.handler.person import PersonRestHandler, PersonListHandler, PersonCreateHandler, PersonDeleteHandler
from com.msb.web.handler.contact import ContactHandler
from com.msb.web.handler.error import Error404Handler, Error500Handler

application = webapp2.WSGIApplication([

    ('/', IndexHandler),

    ('/information', InformationHandler),

    ('/person', PersonRestHandler),
    ('/person/list', PersonListHandler),
    ('/person/create', PersonCreateHandler),
    ('/person/delete', PersonDeleteHandler),

    ('/contact', ContactHandler),

], debug=True)


application.error_handlers[404] = ErrorHandlerAdapter(Error404Handler)
application.error_handlers[500] = ErrorHandlerAdapter(Error500Handler)